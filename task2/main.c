#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

void narisi(int stslike);
int ali_ze_uporabljeno(char poskusene[],int dolz,char crka);
int ali_je_crka(char crka);
void izpis_poskusenih(char poskusene[],int dolz);

int main() {
    
    int stnapak=0,dod,stposkusov=0;
    char crka;
    char mozne_besede[6][10]={"neki","ljubljana","famnit","chips","kozarec","hrana"};       //par besed za uporabo
    char poskusene_crke[28];    //slovenske in angleške črke
    
    //izbere in si zapomne izbrano besedo
    srand(time(NULL));
    int izbranab=rand()%6;
    char beseda[10];
    strcpy(beseda,mozne_besede[izbranab]);

    //naredi zakrito besedo
    char zakrita_besda[strlen(beseda)];
    strcpy(zakrita_besda,beseda);
    for(int j=0;j<strlen(zakrita_besda);j++){
        zakrita_besda[j]='_';
    }

    while(strcmp(zakrita_besda,beseda)==-1){
        dod=0;
        printf("\n\n\n\n\n\n\n\n");
        narisi(stnapak);
        printf("beseda: %s\n",zakrita_besda);
        izpis_poskusenih(poskusene_crke,stposkusov);
        printf("poskusi crko: ");
        fflush(stdout);
        scanf(" %c", &crka);

        while(ali_je_crka(crka)==0){
            printf("vpisi malo crko!!!\n");
            fflush(stdout);
            scanf(" %c", &crka);
        }

        while(ali_ze_uporabljeno(poskusene_crke,stposkusov,crka)==1){
            printf("to crko si ze uporabil\n");
            printf("poskusi drugo crko: ");
            fflush(stdout);
            scanf(" %c", &crka);
        }

        poskusene_crke[stposkusov]=crka;
        stposkusov++;
        for(int i=0;i<strlen(beseda);i++){
            if(beseda[i]==crka){
                zakrita_besda[i]=crka;
                dod=1;
            }
        }

        if(dod==0){
            stnapak++;
            //printf("%i\n",stnapak);
        }

        if(stnapak==6){
            printf("\n\n\n\n\n\n\n\n");
            narisi(stnapak);
            printf("zmankalo ti je poskusov\n");
            return 0;
        }
    }

    printf("\n\n\n\n\n\n\n\n");
    printf("USPELO TI JE\n");
    printf("beseda je: %s\n",zakrita_besda);


  return 0;
}


//narise sliko iz txt fila
void narisi(int stslike){
    char line[55];
    int dod=0;
    char slike[7][55];
    FILE* file=fopen("slike.txt","r");
    
    while(fgets(line,sizeof(line),file)!= NULL){
        strcpy(slike[dod],line);
        dod+=1;
    }

    dod=0;
    for(int i=0;i<strlen(slike[stslike]);i++){
        printf("%c",slike[stslike][i]);
        dod++;
        if(dod==7){
            printf("\n");
            dod=0;
        }
    }

    fclose(file);
}

//izpise poskusene
void izpis_poskusenih(char poskusene[],int dolz){
    printf("%c ",poskusene[0]);
    for(int i=1;i<dolz;i++){
        printf(",%c ",poskusene[i]);
    }
    printf("\n");
}

//preveri ali je bila ze uporabljena
int ali_ze_uporabljeno(char poskusene[],int dolz,char crka){
    int dod=0;
    for(int i=0;i<dolz;i++){
        if(poskusene[i]==crka){
            return 1;
        }
    }
    return 0;
}


//preveri ali je crka
int ali_je_crka(char crka){
    if(crka>96 && crka<123){
        return 1;
   }
    return 0;
}
