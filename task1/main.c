#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

int prever_vpisa(char vpis[]);
int prever_odgovora(char vpis[]);
int ali_vstopijo(int stnad);
int nadst_v_stevilke(char nad[]);
int st_med_nadstropji(char trnad[],char zaznad[]);
int smer_poti(char trnad[],char zaznad[]);

int main(){
srand(time(0));
char tab_nadstropji[9][3]={"B3","B2","B1","p","1","2","3","4","5"};         //narejeno samo za preverjanje zaporedja nadstropji
char dod[3];
char nadstropje[3]="p";     //trenutno nadstropje
char temp1;                 //ko vpisujem v spremljivko 'odgovor' se vsebina nadstropja izbrise
char temp2,temp3;           //zato sem naredil te spremljivke da se zapomne nadstropje 
int stpremikov;
char odgovor[2]="ne";
while(strcmp(odgovor,"ne")){
    printf("\n\n\n\n\n\n\n\n");   
    printf("Ali zelite vstopiti?\n");
    temp1=nadstropje[0];
    temp2=nadstropje[1];
    temp3=nadstropje[2];
    scanf(" %s",odgovor);
    while(prever_odgovora(odgovor)!=1){         //preverja ali je pravilno napisal odgovor
        printf("vpisi 'da' ali 'ne'\n");
        scanf(" %s",odgovor); 
    }
    if(strcmp(odgovor,"da")==0){
        nadstropje[0]=temp1;
        nadstropje[1]=temp2;
        nadstropje[2]=temp3;
        printf("ste na nadstropju: %s\n",nadstropje);
        printf("kam zelite iti?\n");
        strcpy(dod,nadstropje);
        scanf(" %s",nadstropje);
        while(prever_vpisa(nadstropje)!=1 || strcmp(dod,nadstropje)==0){                        //preveri ali je vpisal validno nadstropje in ce je vpisal nadstropje v katerem se trenutno nahaja
            printf("vpisi veljavno nadstropje ali nadstropje v katerem trenutno niste\n");
            scanf(" %s",nadstropje);   
        }
        
        stpremikov=st_med_nadstropji(dod,nadstropje);      

        int smer=smer_poti(dod,nadstropje);
        int zacindex=nadst_v_stevilke(dod)+3;
        for(int i=0;i<stpremikov-1;i++){
            if(smer==1){
                zacindex++;
                if(ali_vstopijo(11)>5 ){
                    printf("ljudje vstopajo/iztopajo na: %s\n",tab_nadstropji[zacindex]);
                }

            }else if(smer==-1){
                zacindex--;
                if(ali_vstopijo(11)>5){
                    printf("ljudje vstopajo/iztopajo na: %s\n",tab_nadstropji[zacindex]);
                }

            }

        }


        
    }

    
}

return 0;
}

//preveri ali je vpis nadstropja veljaven
int prever_vpisa(char vpis[]){
    if(strcmp(vpis,"1") == 0 || strcmp(vpis,"2") == 0 || strcmp(vpis,"3") == 0|| strcmp(vpis,"4")== 0 || strcmp(vpis,"5")== 0 || strcmp(vpis,"p")== 0 || strcmp(vpis,"B1")== 0 || strcmp(vpis,"B2")==0 || strcmp(vpis,"B3") ==0){
        return 1;
    }else{
        return 0;
    } 

}

//preveri alji je odgovor veljaven
int prever_odgovora(char vpis[]) {
    if(strcmp(vpis,"da") == 0 || strcmp(vpis,"ne")==0){
        return 1;
    }else{
        return 0;
    }
}

//nakljucna funkcija ki samo vrne nakljucno stevilo
int ali_vstopijo(int stnad){
    int r=rand()%stnad;
    return r;
}

//vrne stevilo nadstropji med zacetnim in zazeljenim
int st_med_nadstropji(char trnad[],char zaznad[]){
    int dod=nadst_v_stevilke(trnad);
    int dod2=nadst_v_stevilke(zaznad);
    int nad=0;


    if(dod==dod2){
        return 0;
    }else if(dod>dod2){
        while(dod>dod2+nad){
            nad++;
        }
        return nad;
    }else if(dod<dod2){
        while(dod+nad<dod2){
            nad++;
        }
        return nad;
    }
}

//vrne ali gre dvigalo gor ali dol
int smer_poti(char trnad[],char zaznad[]){
    int dod=nadst_v_stevilke(trnad);
    int dod2=nadst_v_stevilke(zaznad);

    if(dod==dod2){
        return 0;
    }else if(dod<dod2){
        return 1;
    }else if(dod2<dod){
        return -1;
    }


}

//samo da pretvori nadstropje v stevilko za lazjo uporabo
int nadst_v_stevilke(char nad[]){
    int dod=0;
    if(strcmp(nad,"p")==0) dod=0;
    else if(strcmp(nad,"B1")==0) dod=-1;
    else if(strcmp(nad,"B2")==0) dod=-2;
    else if(strcmp(nad,"B3")==0) dod=-3;
    else dod=(int)nad[0]-48;
    return dod;
}